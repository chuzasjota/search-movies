import React from 'react'
import { Link } from 'react-router-dom'

export const ButtonBackToHome = () => (
  <Link
    className='btn btn-primary'
    to='/'>
    Volver al inicio
  </Link>
)
