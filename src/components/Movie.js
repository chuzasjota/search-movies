import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'


export class Movie extends React.Component {
  static propTypes = {
    id: PropTypes.string,
    title: PropTypes.string,
    year: PropTypes.string,
    poster: PropTypes.string
  }

  render () {
    const { id, poster, title, year } = this.props

    return (
      <div className="card">
        <img src={poster} className="card-img-top" alt={ title } />
        <div className="card-body">
          <h5 className="card-title">{ title }</h5>
          <p>{year}</p>
          <Link to={`/detail/${id}`} className="btn btn-primary">Ver más</Link>
        </div>
      </div>
    )
  }
}
