import React from 'react'
import PropTypes from 'prop-types'
import { Movie } from './Movie'

export class MoviesList extends React.Component {
  static propTypes = {
    movies: PropTypes.array
  }

  render () {
    const { movies } = this.props
    return (
      <div className="row justify-content-center">
      {
        movies.map(movie => {
          return (
            <div className='col-md-4 col-sm-6' key={movie.imdbID}>
              <Movie
                id={movie.imdbID}             
                title={movie.Title}
                year={movie.Year}
                poster={movie.Poster}
              />
            </div>
          )
        })
      }
      </div>
    )
  }
}
