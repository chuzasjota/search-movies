import React from 'react'

const ApiKey = '78907102'
export class SearchForm extends React.Component {
  state = {
    inputMovie: ''
  }

  _handleChange = (e) => {
    this.setState({ inputMovie: e.target.value })
  }

  _handleSubmit = (e) => {
    e.preventDefault()
    const { inputMovie } = this.state
    fetch(`http://www.omdbapi.com/?apikey=${ApiKey}&s=${inputMovie}`)
      .then(res => res.json())
      .then(results => {
        const { Search = [], totalResults = "0" } = results
        console.log({ Search, totalResults })
        this.props.onResults(Search)
      })
  }

  render() {
    return (
      <form onSubmit={ this._handleSubmit } className="form-inline">
        <input
          className="form-control mr-sm-2"
          type="search"
          placeholder="Search" 
          aria-label="Search"
          onChange={ this._handleChange }
        />
        <button className="btn btn-outline-primary my-2 my-sm-0" type="submit">Search</button>
      </form>
    )
  }
}