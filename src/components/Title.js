import React from 'react'

export const Title = ({ children }) => (
  <span className="navbar-brand mb-0 h1">{children}</span>
)
