import React from 'react'

import { Title } from '../components/Title'
import { SearchForm } from '../components/SearchForm'
import { MoviesList } from '../components/MoviesList'

export class Home extends React.Component {
  state = { usedSearch: false, results: [] }

  _handleResults = (results) => {
    this.setState({ results, usedSearch: true })
  }

  _renderResults () {
    return this.state.results.length === 0
      ? <p>Sorry! 😞 Results not found!</p>
      : <MoviesList movies={this.state.results} />
  }

  render () {
    return (
      <div>
        <nav className="navbar navbar-dark bg-dark">
          <Title>Buscador de películas</Title>
          <SearchForm onResults={ this._handleResults } />
        </nav>
        <div className="container">
          {this.state.usedSearch
            ? this._renderResults()
            : <p>Usa el formulario para encontrar una película</p>
          }
        </div>
      </div>
    )
  }
}
